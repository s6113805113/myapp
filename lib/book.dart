import 'package:flutter/material.dart';

void main() {
  runApp(BooksApp());
}

class Book {
  final String teacher;
  final String subject;

  Book(
    this.teacher,
    this.subject,
  );
}

class BooksApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BooksAppState();
}

class _BooksAppState extends State<BooksApp> {
  Book _selectedBook;

  List<Book> books = [
    Book(
      'การวิจัยทางธุรกิจ',
      'ผศ.ดร.ดวงรัตน์โกยกิจเจริญ',
    ),
    Book('ระบบสารสนเทศเพื่อการจัดการ', 'อาจารย์ พัทธนันท์ เพ็งดำ'),
    Book('การพัฒนาโปรเเกรมประยุกต์บนเว็บ', 'อาจารย์ ภาวิกา ขุนจันทร์'),
    Book('กฎหมายธุรกิจ', 'อาจารย์ มาฤทธิ์ กาญจนารักษ์'),
    Book('การพัฒนาโปรเเกรมประยุกต์สำหรับอุปกรณ์เคลื่อนที่',
        'อาจารย์ วรรัตน์ จงไกรจักร'),
    Book('ภาษาอังกฤษเพื่อการสื่อสารทางธุรกิจ', 'อาจารย์ ขจร ทุ่มศรี'),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chek Name',
      home: Navigator(
        pages: [
          MaterialPage(
            key: ValueKey('BooksListPage'),
            child: BooksListScreen(
              books: books,
              onTapped: _handleBookTapped,
            ),
          ),
          if (_selectedBook != null) BookDetailsPage(book: _selectedBook)
        ],
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }

          // Update the list of pages by setting _selectedBook to null
          setState(() {
            _selectedBook = null;
          });

          return true;
        },
      ),
    );
  }

  void _handleBookTapped(Book book) {
    setState(() {
      _selectedBook = book;
    });
  }
}

class BookDetailsPage extends Page {
  final Book book;

  BookDetailsPage({
    this.book,
  }) : super(key: ValueKey(book));

  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        return BookDetailsScreen(book: book);
      },
    );
  }
}

class BooksListScreen extends StatelessWidget {
  final List<Book> books;
  final ValueChanged<Book> onTapped;

  BooksListScreen({
    @required this.books,
    @required this.onTapped,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CLASS '),
      ),
      body: ListView(
        children: [
          Image.asset(
            'images/4.jpg',
            width: 300,
            height: 240,
            fit: BoxFit.cover,
          ),
          title,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(),
              for (var book in books)
                ListTile(
                  title: Text(book.teacher),
                  subtitle: Text(book.subject),
                  onTap: () => onTapped(book),
                )
            ],
          ),
        ],
      ),
    );
  }
}

class BookDetailsScreen extends StatelessWidget {
  final Book book;

  BookDetailsScreen({
    @required this.book,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(18, 0, 0, 0), //ด้านในหน้าถัดไป
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (book != null) ...[
              Text(book.teacher, style: Theme.of(context).textTheme.headline6),
              Text(book.subject, style: Theme.of(context).textTheme.subtitle1),
              letfcolumn,
            ],
          ],
        ),
      ),
    );
  }
}

Widget title = Container(
  padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
  child: Text(
    'Subjects studied',
    textAlign: TextAlign.center,
    style: TextStyle(
      fontWeight: FontWeight.w800,
      letterSpacing: 0.5,
      fontSize: 30,
    ),
  ),
);
Widget letfcolumn = Container(
  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
  child: Column(
    children: [
      mianImages,
    ],
  ),
);
final mianImages = Container(
  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
  child: Column(
    children: [
      Image.asset(
        'images/2.png',
        width: 400,
        height: 250,
      ),
    ],
  ),
);
final iconList2 = DefaultTextStyle.merge(
  style: descTextStyle,
  child: Container(
    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Icon(Icons.timer, color: Colors.green[500]),
            Text('COOK:'),
            Text('1 hr'),
          ],
        ),
      ],
    ),
  ),
);
final descTextStyle = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w800,
  fontFamily: 'Roboto',
  letterSpacing: 0.5,
  fontSize: 18,
  height: 2,
);

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}
